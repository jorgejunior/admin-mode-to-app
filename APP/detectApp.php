<?php
if($_POST['ftpHostCheck']=='ftp'){
	$host='ftp.'.$_POST['hostFtp'];
}
else{
	$host='web'.$_POST['hostFtp'].'.f1.k8.com.br';

}
$app=null;
$erro=false;
$configDir=null;
$configInstalador=false;
$user=$_POST['ftpUser'];
$pass=$_POST['ftpPass'];
$path=$_POST['path'];

require_once 'ftpCon.php';
$f=new ftpCon();
if($f->connect($host)){

	if($f->login($user,$pass)){
		$items=$f->readDir($path);
		if(in_array('config_instalador.php', $items)){
			$configInstalador=true;
		}
		if(in_array('configuration.php',$items)){
			hiz("Joomla detectado");
			$app='J';
			$configDir=$path.DIRECTORY_SEPARATOR.'configuration.php';

		}
		else{
			if(in_array('wp-config.php',$items)){
				hiz("Wordpress detectado");
				$app='W';
				$configDir=$path.DIRECTORY_SEPARATOR.'wp-config.php';
			}
			else{
				$itemsP=$f->readDir($path.DIRECTORY_SEPARATOR.'config/');
				if(in_array('settings.inc.php',$itemsP)){
					unset($itemsP);
					hiz("Prestashop detectado");
					$app='P';
					$configDir=$path.DIRECTORY_SEPARATOR.'config/settings.inc.php';

				}
				else{
					$itemsP=$f->readDir($path.DIRECTORY_SEPARATOR.'app/etc/');
					if(in_array('local.xml',$itemsP)){
						unset($itemsP);
						hiz("Magento detectado");
						$app='M';
						$configDir=$path.DIRECTORY_SEPARATOR.'app/etc/local.xml';

					}
					else{
						error("Nenhum app detectado");

					}

				}
			}
		}
	}
	else{
		error('Erro ao se conectar utilizando este usuário e senha');

	}
}
else{
	error('Erro ao se conectar com o servidor');
}
$f->logout();
function hiz($e){
	?>
	<div class="alert alert-success bolder alert-app"><a href="#" class="close" data-dismiss='alert'>&times;</a><?= $e;?><br>Carregando senha provisória. <i class="fa fa-spinner fa-pulse"></i></div>
	<?php
}
function error($e){
	?>
	<div class="alert alert-danger bolder alert-app"><a href="#" class="close" data-dismiss='alert'>&times;</a><?= $e;?></div>
	<?php
	$GLOBALS['erro']=true;
}
?>
<script>
	$(document).ready(function() {
		$('.alert-app').slideDown(500);
		setTimeout(function () {
			$.post("defineAppPass.php",{
					"host": "<?= base64_encode($host);?>",
					"user": "<?= base64_encode($user);?>",
					"pass": "<?= base64_encode($pass);?>",
					"app": "<?= $app;?>",
					"path": "<?= $path;?>",
					"configDir": "<?= $configDir;?>",
					"configInstalador": "<?= $configInstalador;?>",
					"newPass": "<?= $_POST['newPass'];?>"
				},
				function(data){
					$('.alert-app').slideUp(500);
					setTimeout(function () {
						$('#res').html(data);

					},500);
				});
		},1000);

	});

	<?php

        if(!$erro){
            ?>

	<?php
}
else{
    ?>
	$btn.attr('disabled',false);

	<?php
}
?>

</script>
