<?php

/**
 * Created by PhpStorm.
 * User: jorge-jr
 * Date: 30/12/15
 * Time: 10:05
 */
class ftpCon
{
    private $ftp;
    public function connect($host){
        $this->ftp=ftp_connect($host);
        return $this->ftp;
    }
    public function login($user,$pass){
        return ftp_login($this->ftp,$user,$pass);
    }
        public function readDir($path){
        $DIR=str_replace('\\ ',' ',$path);
        $DIR=str_replace(' ','\\ ',$DIR);
        $list=null;
        if ($children = @ftp_nlist($this->ftp, $DIR)) {

            foreach ($children as $p) {
                $list[]=$p;
            }
        }
        return $list;

    }
    public function logout(){
        ftp_close($this->ftp);
    }
}