<!doctype html>
<html lang="PT-BR">
<head>
    <meta charset="UTF-8">
    <title>Adm mode</title>
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <style>
        .width50{
            width: 49%;
        }
        .input-group-addon{
            top: 0;
        }
        .bolder{
            font-weight: bolder;
        }
    </style>
    <script>
        $(document).ready(function () {
            checkHost();
            $('[name=ftpHostCheck]').change(checkHost);
                $('.btn-set').click(function () {
                    $btn=$(this);
                    //$btn.attr('disabled',true);
                    $.ajax({
                        url: "detectApp.php",
                        method: "POST",
                        data: $('[name=formAdm]').serialize()
                        }).done(function (data) {
                            $('#res').html(data);



                    });
                });
             window.onbeforeunload=function(e){
                return("Caso algum aplicativo esteja em modo administrativo, a senha será perdida. Deseja continuar ?");
            };
        });
        function checkHost(){
            var y=$('[name=ftpHostCheck]:checked').val();
            if(y=='prv'){
                $('.ftpHost').html('<span class="input-group-addon">web</span><input type="text" class="form-control" id="hostFtp" name="hostFtp"><span class="input-group-addon">.f1.k8.com.br</span>');

            }
            else{
                $('.ftpHost').html('<span class="input-group-addon">ftp.</span><input type="text" class="form-control" id="hostFtp" name="hostFtp">');
            }
        }
    </script>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                    <div class="alert alert-warning bolder">Atenção: Caso saia desta página e deixe a conta no modo administrativo, os dados serão perdidos.</div>

                <form name="formAdm" onsubmit="return false;">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="control-label width50"><input type="radio" value="prv" name="ftpHostCheck"> Provisório</label>
                            <label class="control-label width50"><input type="radio" value="ftp" name="ftpHostCheck" checked> Normal</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="hostFtp" class="control-label">FTP Host</label>
                            <div class="input-group ftpHost"></div>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="ftpUser" class="control-label">FTP User</label>
                            <input type="text" class="form-control" name="ftpUser" id="ftpUser">
                        </div>

                        <div class="form-group col-md-12">
                            <label for="ftpPass" class="control-label">FTP Pass</label>
                            <input type="password" class="form-control" name="ftpPass" id="ftpPass">
                        </div>

                        <div class="form-group col-md-12">
                            <label for="newPass" class="control-label">New Pass</label>
                            <input type="text" class="form-control" name="newPass" id="newPass">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="path" class="control-label">Path</label>
                            <input type="text" class="form-control" name="path" id="path">
                        </div>
                        <div class="form-group col-md-12 btn-toAct">
                            <button type="button" class="btn btn-success btn-set"><i class="fa fa-dashboard"></i> Ativar senha provisória</button>
                        </div>
                    </div>
                    <div id="res"></div>


                    <input type="hidden" name="exPass">
                </form>
            </div>
        </div>


    </div>
</body>
</html>